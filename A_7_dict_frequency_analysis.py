n = int(input())
wordCollection = []
dictResult = {}
for i in range(n):
    s = input().split()
    for j in range(len(s)):
        wordCollection.append(s[j])
for i in range(len(wordCollection)):
    dictResult[wordCollection[i]] = wordCollection.count(wordCollection[i])
for name, occur in sorted(sorted(dictResult.items()), key=lambda key_value: key_value[1], reverse=True):
    print(name, occur)
