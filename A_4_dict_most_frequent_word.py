# Ingresar el numero de lineas
# ingresar las palabras segidas de un espacio
# Determinar la frecuencia de las palabras
# Si hay muchas palabras imprimir la de menor orden alfabetico

diccionario = {}
for _ in range(int(input("¿Cuantas lineas necesitas?"))):
    palabras = input().split()
    i = 0
    for i in range(len(palabras)):
        diccionario[palabras[i]] = diccionario.get(palabras[i], 0) + 1

valorMaximo = max(diccionario.values())
repite = [k for k, v in diccionario.items() if v == valorMaximo]
print(min(repite))
