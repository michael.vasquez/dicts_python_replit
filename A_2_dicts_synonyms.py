# Ingresar el numero de parejas de palabras
# Ingrese las palabras
# Comparar las palabras para hallar su equivalencia
# Imprime el sinonimo que se predetermino

n = int(input("¿Cuantas parejas de palabras quieres\n?"))
stringA = [[str(s) for s in input().split()] for i in range(n)]
wordpairs = {k: v for (k, v) in stringA}
word = input("¿Sinonimo de que palabra buscas\n?")
key = 0
value = 0
for key, value in wordpairs.items():
    if wordpairs[key] == word:
        print(key)
    elif key == word:
        print(wordpairs[key])
